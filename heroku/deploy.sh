#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname "$0")"

: ${HEROKU_APP_NAME}
: ${HEROKU_TOKEN}

GREEN='\033[0;32m'
BLUE='\033[1;34m'
CYAN='\033[1;36m'
NC='\033[0m'

log() {
    printf "${CYAN}>>> ${GREEN}$@${NC}\n"
}
export IMAGE_NAME=registry.heroku.com/${HEROKU_APP_NAME}/worker
log "Building Docker image ${BLUE}$IMAGE_NAME"
docker build -t ${IMAGE_NAME} ..
export IMAGE_ID=$(docker inspect ${IMAGE_NAME} --format='{{.Id}}')
log "Logging in to ${BLUE}registry.heroku.com"
docker login registry.heroku.com --username _ --password-stdin <<< "${HEROKU_TOKEN}"
log "Pushing Docker image to ${BLUE}registry.heroku.com"
docker push ${IMAGE_NAME}
log "Deploying new Docker image with tag ${BLUE}${IMAGE_ID}"
curl -n -X PATCH https://api.heroku.com/apps/${HEROKU_APP_NAME}/formation \
    -d '{ "updates": [ { "type": "worker", "docker_image": "'${IMAGE_ID}'" } ] }' \
    -H "Authorization: Bearer $HEROKU_TOKEN" \
    -H "Content-Type: application/json" \
    -H "Accept: application/vnd.heroku+json; version=3.docker-releases"
