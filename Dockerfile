FROM node:10-alpine as base

RUN apk add --no-cache \
        tini \
        chromium \
        nss
ENTRYPOINT ["/sbin/tini", "-s", "--"]

FROM node:10-alpine as builder

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

COPY . /app
RUN cd /app \
    && npm i \
    && npm run build \
    && npm prune --production

FROM base

WORKDIR /app
RUN adduser -S user && \
    mkdir -p /app/errors && \
    chown user /app/errors
VOLUME ["/app/errors"]
USER user
ENV puppeteer__executablePath=/usr/bin/chromium-browser
ENV puppeteer__args=--no-sandbox
ENV NODE_ENV=production

COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/dist /app/dist

CMD node dist/index.js
