import Discord, { Message, PresenceData, RichEmbed } from 'discord.js';
import { Server, ServerStatus } from './arkservers';

const log = require('log4js').getLogger('discord');

export class DiscordBot {
    client: Discord.Client;
    arkServer: Server;
    token: string;
    channelId: string;
    statusMessage?: {
        embed: RichEmbed;
        message: Message;
        originalMessage: Message;
        type: 'start' | 'stop';
    };

    constructor({
        arkServer,
        token,
        channelId,
    }: {
        arkServer: Server;
        token: string;
        channelId: string;
    }) {
        this.arkServer = arkServer;
        this.token = token;
        this.channelId = channelId;
        this.client = new Discord.Client();
        this.client.on('ready', () =>
            this.onReady().catch(e => log.error(`${this.client.user.tag}: `, e))
        );
        this.client.on('message', msg =>
            this.onMessage(msg).catch(e => log.error(`${this.client.user.tag}: `, e))
        );
    }

    public async run() {
        return await this.client.login(this.token);
    }

    public async close() {
        await this.client.destroy();
    }

    private async onReady() {
        log.info(`${this.client.user.tag}: Logged in!`);

        if (log.isDebugEnabled()) {
            log.debug(`${this.client.user.tag}: Member of the following channels:`);
            this.client.guilds.forEach((v, k) => {
                log.debug(`${this.client.user.tag}: === ${k}: ${v.name}`);
                v.channels.forEach((v, k) => {
                    log.debug(`${this.client.user.tag}:  - ${k}: ${v.name}`);
                });
            });
        }

        await this.client.user.setPresence({
            status: 'dnd',
            game: null,
        });

        try {
            const status = await this.arkServer.status();
            await this.updateBotStatus(status);
        } catch (e) {
            log.error(e);
        }

        this.arkServer.on('status', async status => {
            await this.updateBotStatus(status);
        });
    }

    private async updateBotStatus(status: ServerStatus) {
        const newPresence: PresenceData = {
            status: status.status === 'online' ? 'online' : 'dnd',
            game:
                status.status !== 'stopped'
                    ? {
                          name: `ARK - ${status.status}${
                              status.status === 'online' ? ` (${status.playersOnline})` : ''
                          }`,
                          type: 'PLAYING',
                      }
                    : null,
        };
        const currentPresence = this.client.user.presence;
        if (
            currentPresence.status !== newPresence.status ||
            !!currentPresence.game !== !!newPresence.game ||
            (currentPresence.game &&
                newPresence.game &&
                currentPresence.game.name !== newPresence.game.name)
        ) {
            log.debug(`${this.client.user.tag}: Updating bot presence to:`, status.status);
            await this.client.user.setPresence(newPresence);
        }
        if (this.statusMessage) {
            if (
                (status.status === 'online' && this.statusMessage.type === 'start') ||
                (status.status === 'stopped' && this.statusMessage.type === 'stop')
            ) {
                const statusMessage = this.statusMessage;
                delete this.statusMessage;
                await statusMessage.message.delete();
                await statusMessage.originalMessage.react('👍');
            } else if (status.status !== 'online' && status.status !== 'stopped') {
                await this.statusMessage.message.edit(
                    new RichEmbed(this.statusMessage.embed).setAuthor(
                        status.status,
                        'https://i.imgur.com/MY2pyhX.gif'
                    )
                );
            }
        }
    }

    commands: { [index: string]: (msg: Message) => Promise<void> } = {
        listplayers: async msg => {
            await msg.reply(await (await this.arkServer.rcon()).listplayers());
        },
        saveworld: async msg => {
            await msg.reply(await (await this.arkServer.rcon()).saveworld());
        },
        status: async msg => {
            let status = await this.arkServer.status();
            await msg.reply(
                `${status.status}${
                    status.status === 'online'
                        ? `, ${status.playersOnline} player${
                              status.playersOnline !== 1 ? 's' : ''
                          } online`
                        : ''
                }`
            );
        },
        stop: async msg => {
            if (this.statusMessage) {
                const message = this.statusMessage.message;
                const originalMessage = this.statusMessage.originalMessage;
                delete this.statusMessage;
                await message.delete();
                await originalMessage.react('👎');
            }
            this.statusMessage = await this.createStatusMessage('stop', msg);
            if (!(await this.arkServer.stopServer())) {
                await this.statusMessage.message.delete();
                await msg.react('👎');
            }
        },
        start: async msg => {
            if (this.statusMessage) {
                const message = this.statusMessage.message;
                const originalMessage = this.statusMessage.originalMessage;
                delete this.statusMessage;
                await message.delete();
                await originalMessage.react('👎');
            }
            this.statusMessage = await this.createStatusMessage('start', msg);
            if (!(await this.arkServer.startServer())) {
                await this.statusMessage.message.delete();
                await msg.react('👎');
            }
        },
        help: async msg => {
            await msg.reply(
                `**Available commands:** \n\n${Object.keys(this.commands)
                    .map(k => ` 🔹 !${k}`)
                    .join('\n')}`
            );
        },
    };

    private async createStatusMessage(type: 'start' | 'stop', msg: Message) {
        const embed = new RichEmbed()
            .setColor(type === 'start' ? '#00c100' : '#c10000')
            .setAuthor(
                type === 'start' ? 'starting server' : 'stopping server',
                'https://i.imgur.com/MY2pyhX.gif'
            );
        const message = await msg.channel.send(embed);
        return { type, embed, message, originalMessage: msg };
    }

    private async onMessage(msg: Message) {
        if (msg.channel.id !== this.channelId) {
            return;
        }

        if (msg.content.charAt(0) === '!') {
            const command = this.commands[msg.content.substr(1)];
            if (command) {
                log.debug(
                    `${this.client.user.tag}: Received command '${msg.content}' from ${msg.author.username}`
                );
                msg.channel.startTyping();
                try {
                    await command.call(this, msg);
                } catch (e) {
                    await msg.reply(e.message);
                    log.error(e);
                } finally {
                    msg.channel.stopTyping();
                }
            }
        }
    }
}
