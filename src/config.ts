import fs from 'fs';
import yaml from 'js-yaml';
import nconf from 'nconf';
import path from 'path';

const log = require('log4js').getLogger('ark');

nconf.argv().env({ separator: '__', parseValues: true });

const configDir = nconf.get('configDir');
const configFile = nconf.get('config');

if (fs.existsSync('config.yaml')) {
    console.log(`Loading config.yaml`);
    nconf.file({
        file: 'config.yaml',
        format: {
            parse: yaml.safeLoad,
            stringify: yaml.safeDump,
        },
    });
}

if (configDir) {
    fs.readdirSync(configDir).forEach(file => {
        const fullPath = path.join(configDir, file);
        if (fs.statSync(fullPath).isFile()) {
            console.log(`Loading ${fullPath}`);
            nconf.file(fullPath, {
                file: fullPath,
                format: {
                    parse: yaml.safeLoad,
                    stringify: yaml.safeDump,
                },
            });
        }
    });
}
if (configFile) {
    console.log(`Loading ${configFile}`);
    nconf.file(configFile, {
        file: configFile,
        format: {
            parse: yaml.safeLoad,
            stringify: yaml.safeDump,
        },
    });
}

nconf.defaults({
    logLevel: 'info',
    puppeteer: {
        headless: true,
    },
});

const _ = process.env;

const config: {
    discord: {
        [key: string]: { token: string; guildId: string; channelId: string; arkServer: string };
    };
    ark: { username: string; password: string };
    logLevel: string;
    puppeteer: { headless: boolean; executablePath: string; args: string };
} = nconf.get();

export default config;
