import Arkservers, { Refresher } from './arkservers';
import config from './config';
import { DiscordBot } from './discord';

const log4js = require('log4js');

log4js.configure({
    appenders: { console: { type: 'console' } },
    categories: { default: { appenders: ['console'], level: config.logLevel } },
});

const log = log4js.getLogger();

async function run() {
    const ark = new Arkservers(config.ark);
    await Promise.all(
        Object.values(config.discord).map(async discord => {
            const arkServer = ark.server(discord.arkServer);
            new Refresher(arkServer);
            const discordBot = new DiscordBot({ ...discord, arkServer });

            process.on('exit', async () => {
                await discordBot.close();
            });

            await discordBot.run();
        })
    );
}
run().catch(e => {
    log.error(e);
    // Containers should restart automatically - just exit
    process.exit(1);
});

process.on('unhandledRejection', error => {
    log.error(error);
    throw error;
});
