import { EventEmitter } from 'events';
import memoize from 'fast-memoize';
import * as fs from 'fs';
import puppeteer, { Browser, ElementHandle, Page } from 'puppeteer';
import config from './config';
const log = require('log4js').getLogger('ark');
import Timer = NodeJS.Timer;

const LOW_REFRESH_STATUSES = ['online', 'stopped'];

export class Refresher {
    private ark: Server;
    private status: string = '';
    private nextRun?: Timer;

    constructor(ark: Server) {
        this.ark = ark;
        this.ark.on('status', status => {
            this.updateStatus(status);
        });
        this.scheduleUpdate();
    }

    scheduleUpdate() {
        const timeout = LOW_REFRESH_STATUSES.includes(this.status) ? 60000 : 5000;
        log.trace(`Scheduling next status update in ${timeout / 1000} seconds`);
        if (this.nextRun) {
            clearTimeout(this.nextRun);
        }
        this.nextRun = setTimeout(async () => {
            try {
                log.trace('Running scheduled status update');
                await this.ark.status();
            } catch (e) {
                log.error(e);
            }
        }, timeout);
    }

    updateStatus(status: ServerStatus) {
        this.status = status.status;
        this.scheduleUpdate();
    }

    stop() {
        if (this.nextRun) {
            clearTimeout(this.nextRun);
        }
    }
}

export default class Arkservers {
    username: string;
    password: string;
    loggedIn: boolean;
    browser!: Browser;
    cookies: puppeteer.Cookie[] = [];

    constructor({ username, password }: { username: string; password: string }) {
        this.username = username;
        this.password = password;
        this.loggedIn = false;
    }

    private ensureBrowser = runOnce(async () => {
        if (!this.browser) {
            log.debug('Starting browser');
            this.browser = await puppeteer.launch({
                executablePath: config.puppeteer.executablePath,
                headless: config.puppeteer.headless,
                args: config.puppeteer.args ? config.puppeteer.args.split(' ') : undefined,
            });
        }
        return this.browser;
    });

    private doLogin = runOnce(async () => {
        log.debug('Logging in to arkservers.io');
        const page = await this.newPage();
        try {
            await screenshotOnError(
                waitOnCloudflareDdosPage(async page => {
                    await page.goto('https://arkservers.io/login');
                    await page.type('input[name=email]', this.username);
                    await page.type('input[name=password]', this.password);
                    await page.evaluate(() =>
                        // @ts-ignore
                        document.querySelector('button[type=submit].btn-success').click()
                    );
                    await page.waitForNavigation();
                    this.loggedIn = await this.isLoggedIn(page);

                    if (!this.loggedIn) {
                        throw new Error('Login failed');
                    }
                    this.cookies = await page.cookies();
                    log.info(`Logged in as ${this.username}!`);
                })
            )(page);
        } finally {
            await page.close();
        }
    });

    private async ensureLoggedIn(url: string) {
        await this.ensureBrowser();
        log.trace("Ensuring that we're logged in");
        const newPage = await this.newPage();
        try {
            await newPage.goto(url);
            if (await this.isLoggedIn(newPage)) {
                log.trace('Already logged in');
                return newPage;
            }

            await this.doLogin();

            await newPage.goto(url);
        } catch (e) {
            await newPage.close();
            throw e;
        }
        return newPage;
    }

    private async newPage() {
        try {
            return await this.browser.newPage();
        } catch (e) {
            log.warn('Failed to open new page. Resetting browser.');
            try {
                await this.browser.close();
            } catch (ignored) { }
            delete this.browser;
            await this.ensureBrowser();
            return await this.browser.newPage();
        }
    }

    async withPage<T>(lambda: (page: Page) => T): Promise<T> {
        return await this.withUrl('https://arkservers.io/panel', lambda);
    }

    async withUrl<T>(url: string, lambda: (page: Page) => T): Promise<T> {
        const page = await this.ensureLoggedIn(url);
        try {
            return await lambda.call(this, page);
        } catch (e) {
            log.debug('Closing browser because of exception', e);
            this.browser.close();
            delete this.browser;
            throw e;
        } finally {
            if (this.browser) {
                await page.close();
            }
        }
    }

    async isLoggedIn(page: Page) {
        return (await page.$x("//button[contains(text(), 'Sign Out')]")).length > 0;
    }

    server(name: string) {
        return new Server(this, name);
    }
}

class RconInterface {
    private ark: Arkservers;
    private pageUrl: string;

    constructor(ark: Arkservers, pageUrl: string) {
        this.ark = ark;
        this.pageUrl = pageUrl;
    }

    listplayers = runOnce(async () => {
        return await this.ark.withUrl(this.pageUrl, async page => {
            const ret = await this.runRconCommand(page, 'listplayers');
            if (/no players connected/i.test(ret.result)) {
                return [];
            }
            return ret.result.split('\n');
        });
    });

    saveworld = runOnce(async () => {
        return await this.ark.withUrl(this.pageUrl, async page => {
            const ret = await this.runRconCommand(page, 'saveworld');
            return ret.result.split('\n');
        });
    });

    private async runRconCommand(page: Page, command: string) {
        log.trace(`RCON: running command: ${command}`);
        return await page.evaluate(
            async (pageUrl: string, command: string) => {
                // @ts-ignore
                const data = new window.FormData();
                // @ts-ignore
                data.append('csrf', window.csrf);
                data.append('command', command);
                return (await window.fetch(new Request(pageUrl), {
                    method: 'POST',
                    body: data,
                })).json();
            },
            this.pageUrl,
            command
        );
    }
}

export declare interface Server {
    on(event: 'status', listener: (status: ServerStatus) => void): this;
}

export class Server extends EventEmitter {
    private readonly ark: Arkservers;
    private readonly name: string;
    private rconUrl?: string;

    constructor(ark: Arkservers, name: string) {
        super();
        this.ark = ark;
        this.name = name;
    }

    rcon = memoize(async () => {
        return new RconInterface(this.ark, await this.getRconPageUrl());
    });

    status: () => Promise<ServerStatus> = runOnce(async () => {
        log.trace(`${this.name}: Fetching status`);
        const status = await this.ark.withPage(
            screenshotOnError(
                waitOnCloudflareDdosPage(async page => {
                    const server = await this.getServerDiv(page);
                    return await {
                        status: await this.getStatusFromDiv(page, server),
                        playersOnline: parseInt(await this.getPlayerCountFromDiv(page, server)),
                    };
                })
            )
        );
        log.trace(`${this.name}: Fetched status`, status);
        this.emit('status', status);
        return status;
    });

    startServer = runOnce(async () => {
        log.info(`${this.name}: Starting server`);
        return await this.ark.withPage(
            screenshotOnError(
                waitOnCloudflareDdosPage(async page => {
                    const server = await this.getServerDiv(page);
                    const status = await this.getStatusFromDiv(page, server);
                    log.trace(`${this.name}: Status when starting server:`, status);
                    await page.evaluateHandle(button => {
                        if (!button.classList.contains('disabled')) {
                            button.click();
                        } else {
                            throw new Error(`${button.getAttribute('data-original-title')}`);
                        }
                    }, await this.getManageButton(server));
                    await page.waitForNavigation();
                    const button = await page.$x(`//button[contains(., 'Start')]`);
                    if (button.length === 1) {
                        await page.evaluateHandle(button => button.click(), button[0]);
                        await page.waitForNavigation();
                        // update status
                        this.status().catch(e => log.error(e));
                        log.debug(`${this.name}: Starting server finished`);
                        return true;
                    }
                    log.warn(`${this.name}: Starting server failed!`);
                    return false;
                })
            )
        );
    });

    stopServer = runOnce(async () => {
        log.info(`${this.name}: Stopping server`);
        return await this.ark.withPage(
            screenshotOnError(async page => {
                const server = await this.getServerDiv(page);
                const status = await this.getStatusFromDiv(page, server);
                log.trace(`${this.name}: Status when stopping server:`, status);
                await page.evaluateHandle(button => {
                    if (!button.classList.contains('disabled')) {
                        button.click();
                    } else {
                        throw new Error(`${button.getAttribute('data-original-title')}`);
                    }
                }, await this.getManageButton(server));
                await page.waitForNavigation();
                const button = await page.$x(`//button[contains(., 'Stop')]`);
                if (button.length === 1) {
                    await page.evaluateHandle(button => button.click(), button[0]);
                    await page.waitForNavigation();
                    // update status
                    this.status().catch(e => log.error(e));
                    log.debug('Stopping server finished');
                    return true;
                }
                log.warn(`${this.name}: Stopping server failed!`);
                return false;
            })
        );
    });

    private getRconPageUrl = runOnce(async () => {
        if (!this.rconUrl) {
            this.rconUrl = await this.ark.withPage(async page => {
                const button = await this.getManageButton(await this.getServerDiv(page));
                await page.evaluateHandle(button => button.click(), button);
                await page.waitForNavigation();
                const url = await page.evaluate(() => window.location.href);
                if (!/\/panel\/servers\/(\d+)$/.test(url)) {
                    throw new Error(`${this.name}: URL` + url + " didn't match expected pattern");
                }
                return url + '/console';
            });
        }
        return this.rconUrl;
    });

    private async getServerDiv(page: Page) {
        const elements = await page.$x(
            `//div[@class='server-list-item' and .//a[@class='hostname' and contains(text(),'${this.name}.arkers.io')]]`
        );
        if (elements.length === 1) {
            return elements[0];
        }
        throw new Error(`Couldn't find server: ${this.name}`);
    }

    private async getPlayerCountFromDiv(page: Page, el: ElementHandle) {
        const playersElements = await el.$x(`.//tr[th[contains(text(), 'Players')]]/td`);
        if (playersElements.length === 1) {
            let matches = /(\d+) \/ \d+/.exec(
                await page.evaluate(e => e.innerText, playersElements[0])
            );
            if (matches != null) {
                return matches[1];
            }
        }
        throw new Error(`${this.name}: Couldn't find player count`);
    }

    private async getStatusFromDiv(page: Page, el: ElementHandle) {
        const statusRow = await el.$x(`.//tr[th[contains(text(), 'Status')]]/td`);
        if (statusRow.length === 1) {
            return await page.evaluate(e => e.innerText, statusRow[0]);
        }
        throw new Error(`${this.name}: Couldn't find server status`);
    }

    private async getManageButton(serverDiv: ElementHandle) {
        let buttons = await serverDiv.$x(
            `.//*[contains(concat(' ', normalize-space(@class), ' '), ' btn ') and contains(., 'Manage')]`
        );
        if (buttons.length !== 1) {
            throw new Error(`${this.name}: Couldn't find Manage button: ${buttons}`);
        }
        return buttons[0];
    }
}

function runOnce<T>(fn: () => Promise<T>): () => Promise<T> {
    let promise: Promise<T> | false = false;
    return () => {
        if (!promise) {
            promise = fn
                .call(null)
                .then((v: T) => {
                    promise = false;
                    return v;
                })
                .catch(e => {
                    promise = false;
                    throw e;
                });
        }
        return <Promise<T>>promise;
    };
}

export interface ServerStatus {
    status: string;
    playersOnline: number;
}

function screenshotOnError<T>(fn: (x: Page) => Promise<T>): (x: Page) => Promise<T> {
    return async page => {
        try {
            return await fn.call(null, page);
        } catch (e) {
            const time = new Date().getTime();
            await page.screenshot({
                path: `errors/page-${time}.png`,
                type: 'png',
            });
            const html = await page.evaluate(async () => document.documentElement.outerHTML);
            fs.writeFileSync(`errors/page-${time}.htm`, html);
            throw e;
        }
    };
}

function waitOnCloudflareDdosPage<T>(fn: (x: Page) => Promise<T>): (x: Page) => Promise<T> {
    return async page => {
        try {
            return await fn.call(null, page);
        } catch (e) {
            try {
                let i = 0;
                while ((await page.$('.cf-browser-verification')) !== null && i++ < 5) {
                    log.debug(`On CloudFlare Anti DDoS page, waiting for navigation...`);
                    await page.waitForNavigation();
                }
                if (i < 5) {
                    return await fn.call(null, page);
                }
            } catch (e) {
                log.debug(`Could not wait for CloudFlare Anti DDoS page`, e);
            }
            throw e;
        }
    };
}
