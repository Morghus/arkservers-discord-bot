import { EventEmitter } from 'events';
import { ServerStatus } from './arkservers';
export { ServerStatus } from './arkservers';
import Timer = NodeJS.Timer;

export class Refresher {
    constructor(ark: Server) {}
}

export default class Arkservers {
    constructor({ username, password }: { username: string; password: string }) {}
    server(name: string) {
        return new Server(this, name);
    }
}

export class Server extends EventEmitter {
    interval?: Timer;
    loading = 0;
    serverStatus: ServerStatus = {
        playersOnline: 0,
        status: 'stopped',
    };

    constructor(ark: Arkservers, name: string) {
        super();
    }

    async rcon(): Promise<RconInterface> {
        return new RconInterface();
    }

    async status(): Promise<ServerStatus> {
        return {
            playersOnline: 0,
            status: 'offline',
        };
        setInterval;
    }

    async startServer() {
        if (this.serverStatus.status !== 'stopped') {
            return false;
        }
        this.loading = 0;
        this.interval = setInterval(() => {
            this.loading += 11;
            if (this.loading >= 100) {
                this.loading = 0;
                clearInterval(this.interval!);
                this.serverStatus = { playersOnline: 0, status: 'online' };
            } else {
                this.serverStatus = {
                    playersOnline: 0,
                    status: `loading server (${this.loading}%)`,
                };
            }
            this.emit('status', this.serverStatus);
        }, 1000);
        return true;
    }

    async stopServer() {
        if (this.serverStatus.status !== 'online') {
            return false;
        }
        this.interval = setInterval(() => {
            this.loading += 11;
            if (this.loading >= 100) {
                this.loading = 0;
                clearInterval(this.interval!);
                this.serverStatus = { playersOnline: 0, status: 'stopped' };
            } else {
                this.serverStatus = {
                    playersOnline: 0,
                    status: `stopping server (${this.loading}%)`,
                };
            }
            this.emit('status', this.serverStatus);
        }, 1000);
        return true;
    }
}

export class RconInterface {
    async listplayers(): Promise<void> {}
    async saveworld(): Promise<void> {}
}
