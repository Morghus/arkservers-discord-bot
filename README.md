# [arkservers.io](http://arkservers.io) Discord Bot

## Running locally

```
$ npm start
```

## Deployment on Kubernetes

-   On the first run, after customizing `config.yaml` and `config-secrets.yaml`:

    ```
    $ KUBE_NAMESPACE=<namespace> DOCKER_HUB_USERNAME=<username> CONFIG_FILE=config.yaml SECRETS_FILE=config-secrets.yaml kube/deploy.sh
    ```

-   Apply deployment:

    ```
    $ KUBE_NAMESPACE=<namespace> DOCKER_HUB_USERNAME=<username> kube/deploy.sh
    ```

## Deployment on Heroku

-   Create Heroku app

    ```
    $ heroku create
    ```

    Note down the app name.

-   Create (or reuse) Heroku API token:

    ```
    $ heroku authorizations:create -d "arkservers-discord-bot"
    ```

    Note down the 'Token:' value.

-   Run the `heroku/deploy.sh` script with the environment variables:

    ```
    $ HEROKU_APP_NAME=<app_name> HEROKU_TOKEN=<token> heroku/deploy.sh
    ```

## Creating a bot

Create a new app and bot at [Discord Developers](https://discordapp.com/developers/applications/)

## Adding a bot to a server

    https://discordapp.com/oauth2/authorize?client_id=[client_id]&scope=bot&permissions=510016
