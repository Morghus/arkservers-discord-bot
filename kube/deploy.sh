#!/usr/bin/env bash
set -euo pipefail

CURRENT_DIR="$PWD"
cd "$(dirname "$0")"

: ${DOCKER_HUB_USERNAME:=}
: ${DOCKER_HUB_PASSWORD:=}
: ${IMAGE_TAG:=$(git rev-parse HEAD)}
: ${KUBE_CLUSTER:=}
: ${KUBE_CA:=}
: ${KUBE_USER_CRT:=}
: ${KUBE_USER_PK:=}
SECRETS_FILE="${SECRETS_FILE+"$CURRENT_DIR/$SECRETS_FILE"}"
CONFIG_FILE="${CONFIG_FILE+"$CURRENT_DIR/$CONFIG_FILE"}"
: ${KUBE_NAMESPACE}

GREEN='\033[0;32m'
BLUE='\033[1;34m'
CYAN='\033[1;36m'
RED='\033[1;31m'
YELLOW='\033[1;33m'
NC='\033[0m'

log() {
	printf "${CYAN}>>> ${GREEN}$@${NC}\n" >&2
}

cleanup_cmds=()

function cleanup() {
    local pos=${#cleanup_cmds[*]}
    cleanup_cmds[$pos]="$*"
	trap do_cleanup EXIT
}
function do_cleanup() {
    for i in "${cleanup_cmds[@]}"; do
        eval ${i}
    done
}

if [ -n "$CONFIG_FILE" -a ! -f "$CONFIG_FILE" ]; then
	printf "${RED}!!! ${YELLOW}Could not read '$CONFIG_FILE'!${NC}\n" >&2
	exit 1
fi

if [ -n "$SECRETS_FILE" -a ! -f "$SECRETS_FILE" ]; then
	printf "${RED}!!! ${YELLOW}Could not read '$SECRETS_FILE'!${NC}\n" >&2
	exit 1
fi

if [ -n "$DOCKER_HUB_USERNAME" ]; then
	export IMAGE_NAME=${DOCKER_HUB_USERNAME}/arkservers-discord-bot:${IMAGE_TAG}
	log "Building docker image ${BLUE}$IMAGE_NAME"
	docker build -t ${IMAGE_NAME} --cache-from "${DOCKER_HUB_USERNAME}/arkservers-discord-bot:base" --cache-from "$IMAGE_NAME" ..
	export IMAGE_ID=$(docker inspect ${IMAGE_NAME} --format='{{.Id}}')
	if [ -n "$DOCKER_HUB_PASSWORD" ]; then
		log "Logging in to ${BLUE}DockerHub"
		docker login --username "$DOCKER_HUB_USERNAME" --password-stdin <<< "$DOCKER_HUB_PASSWORD"
	fi
	log "Pushing image to ${BLUE}DockerHub"
	docker push ${IMAGE_NAME}
fi

if [ -n "$KUBE_CLUSTER" -a -n "$KUBE_CA" -a -n "$KUBE_USER_CRT" -a -n "$KUBE_USER_PK" ]; then
	log "Configuring kubernetes cluster"
	base64 -d <<< "$KUBE_CA" > ca.crt
	cleanup rm ca.crt
	kubectl config set-cluster default --server=${KUBE_CLUSTER} --certificate-authority=ca.crt
	base64 -d <<< "$KUBE_USER_CRT" > admin.crt
	cleanup rm admin.crt
	base64 -d <<< "$KUBE_USER_PK" > admin.key
	cleanup rm admin.key
	kubectl config set-credentials admin --client-certificate=admin.crt --client-key=admin.key
	kubectl config set-context default --cluster=default --user=admin
	kubectl config use-context default
fi

log "Updating kubernetes deployment"

kubectl apply -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: ${KUBE_NAMESPACE}
EOF

if [ -n "$SECRETS_FILE" ] && [ -n "$CONFIG_FILE" ]; then
	(
	SECRETS_YAML="$(cat "${SECRETS_FILE}")"
    CONFIG_YAML="$(cat "${CONFIG_FILE}")"
	eval $'cat <<EOF\n'"$(cat vars.yaml)"$'\nEOF\n' | kubectl -n "${KUBE_NAMESPACE}"  apply -f -
	)
fi

if [ -n "${IMAGE_NAME:=}" ]; then
	(
	eval $'cat <<EOF\n'"$(cat deployment.yaml)"$'\nEOF\n' | kubectl -n "${KUBE_NAMESPACE}"  apply -f -
	)
fi
